#ifndef LAB_IMAGE_STRUCT_H
#define LAB_IMAGE_STRUCT_H
#include<stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};
struct image rotate_left(const struct image image);

struct image rotate_right(const struct image image);
#endif //LAB_IMAGE_STRUCT_H
