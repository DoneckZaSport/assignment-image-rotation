#ifndef LAB_UTIL_H
#define LAB_UTIL_H

#include <stdint.h>
enum rotation_side {
    LEFT = 0,
    RIGHT = 1
};
void change_path(char* output_path);
struct image rotate(struct image original);
#endif //LAB_UTIL_H
