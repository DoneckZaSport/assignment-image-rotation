#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "image_struct.h"



struct image rotate_left(const struct image  original ){
    const size_t width = original.height;
    const size_t height = original.width;
    struct pixel* data = malloc(width*height*sizeof(struct pixel));
        for (size_t i = 0; i < width * height; i++) {
            const
            uint64_t col = i % original.width,
                    row = i / original.width,
                    newColumn = original.height - row - 1,
                    newRow = col;
            data[newRow * original.height + newColumn] = original.data[i];
        }
    struct image rotated = {width, height, data};
    printf("Done.\n");
    return rotated;
}

struct image rotate_right(const struct image  original){
    const size_t width = original.height;
    const size_t height = original.width;
    struct pixel* data = malloc(width*height*sizeof(struct pixel));
        for (size_t i = 0; i < width * height; i++) {
            const
            uint64_t col = i % original.width,
                    row = i / original.width,
                    newColumn = row ,
                    newRow = original.width-col-1;
            data[newRow * original.height + newColumn] = original.data[i];

    }
    struct image rotated = {width, height, data};
    return rotated;
}