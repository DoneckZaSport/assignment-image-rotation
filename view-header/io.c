#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "io.h"

#pragma pack(push, 1)
  struct BmpHeader {
    uint16_t bfType; // сделать проверку на bm
    uint32_t  bfileSize;//размер файла
    uint32_t bfReserved;
    uint32_t bOffBits; //Положение пиксельных данных относительно начала данной структуры (в байтах).
    uint32_t biSize; //Размер данной структуры в байтах, указывающий также на версию структуры (здесь должно быть значение 12).
    uint32_t biWidth; //ширина
    uint32_t  biHeight; //высота
    uint16_t  biPlanes; // тока 1
    uint16_t biBitCount; //колво бит на пиксель
    uint32_t biCompression; //	Указывает на способ хранения пикселей
    uint32_t biSizeImage;  //Размер пиксельных данных в байтах. Может быть обнулено если хранение осуществляется двумерным массивом.
    uint32_t biXPelsPerMeter; //Количество пикселей на метр по горизонтали и вертикали
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed; //	Размер таблицы цветов в ячейках
    uint32_t  biClrImportant; //Количество ячеек от начала таблицы цветов до последней используемой (включая её саму).
};

#pragma pack(pop)
enum read_status read_header(FILE* const file_path, struct BmpHeader* header) {
    fread(header, sizeof(struct BmpHeader), 1, file_path);
    if (header->bfType != 0x4D42)
        return READ_INVALID_SIGNATURE;
    if(header->biBitCount != 24)
        return READ_INVALID_HEADER;
    if(header->bOffBits != sizeof(struct BmpHeader))
        return READ_INVALID_BITS;
    return READ_OK;
}
enum read_status read_pixels(FILE* const file_path, struct BmpHeader* const header, struct pixel* const data,int64_t padding) {
    const uint32_t w = header->biWidth;
    const uint32_t h = header->biHeight;
    for (uint32_t i= 0; i < h; i++) {
        int64_t kol = fread(data + i*w, sizeof(struct pixel), w, file_path);
        if (kol != w) {
            return READ_INVALID_PIXELS;
        }
        if (fseek(file_path, padding, SEEK_CUR)!=0) return READ_INVALID_CUR;
    }
    return READ_OK;
}
int64_t padding_calc(struct image const* img){
    return (4 - (img->width * sizeof(struct pixel)) % 4) % 4;
}
struct BmpHeader generate_header(struct image const* img,int64_t padding){
    return (struct BmpHeader){
            .bfType = 0x4D42,
            .bfileSize = sizeof(struct BmpHeader)+(img->width*sizeof(struct pixel)+padding)*img->height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct BmpHeader),
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = img->height*(img->width*sizeof(struct pixel)+img->width%4),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    const int64_t padding  = padding_calc(img);
    struct BmpHeader header = generate_header(img,padding);
    if (fwrite(&header, sizeof (struct BmpHeader), 1, out)<1) return WRITE_ERROR;
    uint8_t ostatki_sladki = {0};
    for (uint64_t i = 0; i < img->height; ++i) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width)
            return WRITE_ERROR;
        if (fwrite(&ostatki_sladki, sizeof(char), padding, out) != padding)
            return WRITE_ERROR;
    }
    return WRITE_OK;

}
enum read_status from_bmp(FILE* const file_path, struct image* const img) {
    struct BmpHeader header;
    enum read_status error = read_header(file_path, &header);
    if (error) return error;
    const int64_t height = header.biHeight;
    const int64_t width = header.biWidth;
    const int64_t off = header.bOffBits;
    fseek(file_path, off, SEEK_SET); // смещение до пикселей
    img->data = malloc(height * width * sizeof(struct pixel));
    img->height = height;
    img->width = width;
    const int64_t padding  = padding_calc(img);
    error = read_pixels(file_path, &header, img->data,padding);
    if (error) free(img->data);
    return error;
}