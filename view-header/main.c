#include <stdio.h>
#include <string.h>
#include "image_struct.h"
#include "util.h"
#include "io.h"

int main() {
    printf("Hello,I'm \"ZvereNaklonitel'3000\" \nI was created by student, to rotate your bmp image!\n");
    char input_path[100];
    FILE* file_ = NULL;
    while (file_ == NULL) {
        printf("Enter path to ur image.\n~ ");
        fgets(input_path,sizeof(input_path),stdin);
        input_path[strcspn(input_path, "\r\n")]=0;
        printf("So,ur image is %s\n", input_path);
        file_ = fopen(input_path, "r+b");
        if (file_ == NULL) {
            printf("!! BMP was not found !!\n");
        }
    }
    char *output_path  = input_path;
    change_path(output_path);
    printf("The output file will be  %s\n", output_path);
    struct image img;
    enum read_status err = from_bmp(file_, &img);
    fclose(file_);
    if (err) {
        return err;
    }
    struct  image  rotated_img = rotate(img);
    FILE* new_img_file = fopen(output_path, "w+b");
    if (new_img_file == NULL) return WRITE_ERROR;
    if (to_bmp(new_img_file, &rotated_img))
    	printf(" !! Rotation error! !! \n");
    fclose(new_img_file);
    printf("My mission has done,goodbye user...\n");
    return 0;
}
