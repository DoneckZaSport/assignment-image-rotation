#include "image_struct.h"
#include <stdbool.h>
#include <stdio.h>
#include "util.h"
#include <string.h>

void change_path(char* output_path) {
    output_path[strcspn(output_path, ".")]=0;
    strcat(output_path, "_rotated.bmp");

}
struct image rotate(struct image const original){
    bool flag = true;
    int8_t ans;
    while (flag){
        printf("Enter \"1\" for clockwise rotation or \"2\" for counterclockwise.\n~ ");
        ans = getchar();
        getchar();
        if (ans==49||ans==50) flag = false;
        else printf("Wrong aswer!\n");
    }
    if (ans==50) return rotate_left(original) ;
    else return rotate_right(original);
}




