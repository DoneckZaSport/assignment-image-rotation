#ifndef LAB_IO_H
#define LAB_IO_H
#include <stdint.h>
#include "image_struct.h"


enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR = 1
};


enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PIXELS,
    READ_INVALID_CUR

};
enum write_status to_bmp(FILE*, const struct image* const);
enum read_status from_bmp(FILE* const, struct image* const);

#endif //LAB_IO_H
